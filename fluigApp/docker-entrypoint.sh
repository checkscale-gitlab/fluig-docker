#!/bin/sh
set -e
export MYSQL_ROOT_PASSWORD_FILE="$(cat /run/secrets/mysql-root-pass)"
export FLUIG_DATABASE_USER="$(cat /run/secrets/mysql-user-name)"
export FLUIG_DATABASE_PASSWORD="$(cat /run/secrets/mysql-user-pass)"
export KEYSTORE_PASSWORD="$(cat /run/secrets/keystore-pass)"
export vFLUIG_HOST_IP="$(awk 'END{print $1}' /etc/hosts)"
destination="/fluig/appserver/domain/configuration/host.xml"
search=vKEYSTORE_PASSWORD
replace=$KEYSTORE_PASSWORD
sed -i "s|${search}|${replace}|" ${destination}
search=vFLUIG_MEMORY_MAX
replace=$FLUIG_MEMORY_MAX
sed -i "s|${search}|${replace}|" ${destination}
search=vFLUIG_MEMORY_MIN
replace=$FLUIG_MEMORY_MIN
sed -i "s|${search}|${replace}|" ${destination}
search=vFLUIG_HOST_ADDRESS_MANAGEMENT
replace=$FLUIG_HOST_ADDRESS_MANAGEMENT
sed -i "s|${search}|${replace}|" ${destination}
destination="/fluig/appserver/domain/configuration/domain.xml"
sed -i "s/${search}/${replace}/" ${destination}
search=vFLUIG_HOST_IP
replace=$vFLUIG_HOST_IP
sed -i "s|${search}|${replace}|" ${destination}
search=vFLUIG_DATABASE_USER
replace=$FLUIG_DATABASE_USER
sed -i "s|${search}|${replace}|" ${destination}
search=vFLUIG_DATABASE_PASSWORD
replace=$FLUIG_DATABASE_PASSWORD
sed -i "s|${search}|${replace}|" ${destination}
search=vFLUIG_DATABASE_MIN_POOL_SIZE
replace=$FLUIG_DATABASE_MIN_POOL_SIZE
sed -i "s|${search}|${replace}|" ${destination}
search=vFLUIG_DATABASE_MAX_POOL_SIZE
replace=$FLUIG_DATABASE_MAX_POOL_SIZE
sed -i "s|${search}|${replace}|" ${destination}
echo EvitandoBugRM >> "/fluig/appserver/console.log"
rm -rf /fluig/appserver/fluig.pid || true
sleep 10
service fluig start -D
sleep 60
service fluig restart -D
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start fluig: $status"
  exit $status
fi
if [ -z "$(ls /wcmdir)" ]; then
echo "Primeira vez iniciando"
echo "Aguardando 8 minutos para mover wcmdir"
sleep 480
echo "Parando fluig para mover wcmdir"
service fluig stop -D
echo "Movendo wcmdir"
rm -f /company/.gitkeep
mv -f /fluig/repository/wcmdir/* /wcmdir/
destination="/fluig/appserver/domain/configuration/domain.xml"
search=/fluig/appserver/apps
replace=/apps
sed -i "s|${search}|${replace}|" ${destination}
search=/fluig/repository/wcmdir
replace=/wcmdir
sed -i "s|${search}|${replace}|" ${destination}
echo "Reiniciando Fluig"
service fluig start -D
else
   echo "wcmdir já com arquivos"
fi
tail -f /fluig/appserver/domain/servers/fluig1/log/server.log
exec "$@"
